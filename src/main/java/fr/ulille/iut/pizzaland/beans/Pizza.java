package fr.ulille.iut.pizzaland.beans;

import java.util.Arrays;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private UUID id = UUID.randomUUID();
    private String name;
    private String base;
    
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public static PizzaDto toDto(Pizza pizza) {
		PizzaDto dto = new PizzaDto();
		dto.setName(pizza.getName());
		dto.setId(pizza.getId());
		dto.setBase(pizza.getBase());
		
		return dto;
	}
	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		pizza.setId(dto.getId());
		pizza.setBase(dto.getBase());
		return pizza;
	}
	public static PizzaCreateDto toCreateDto(Pizza pizza){
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());
		dto.setBase(pizza.getBase());
		
		return dto;
	}
	public static Pizza fromPizzaCreateDto(PizzaCreateDto pizzaCreateDto){
		Pizza pizza = new Pizza();
		pizza.setName(pizzaCreateDto.getName());
		pizza.setBase(pizzaCreateDto.getBase());
		return pizza;
	}
}
