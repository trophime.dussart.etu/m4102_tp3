package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());

	private PizzaDao dao;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}


	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createTable();
		dao.createAssociationTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTable();
		dao.dropPizzaIngredientAssociationTable();
	}

	@Test
	public void testGetEmptyList() {
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/Pizzas").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<PizzaDto> Pizzas;
		Pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {});
		
		assertEquals(0,Pizzas.size());
	}
	
	

	@Test
	public void testExistingPizza(){
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		pizza.setBase("Tomate");
		dao.insert(pizza);

		Response response = target("/Pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(),response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}
	
	  @Test
	  public void testGetNotExistingPizza() {
	    Response response = target("/Pizzas").path(UUID.randomUUID().toString()).request().get();
	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	  }
	  
	   @Test
	    public void testCreatePizza() {
	        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
	        pizzaCreateDto.setName("English CopyBook");
	        pizzaCreateDto.setBase("Crème");

	        Response response = target("/Pizzas").request().post(Entity.json(pizzaCreateDto));

	        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

	        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

	        assertEquals(target("/Pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
	        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	    }

	    @Test
	    public void testCreateSamePizza() {
	        Pizza pizza = new Pizza();
	        pizza.setName("Chorizo");
	        pizza.setBase("Tomate");
	        dao.insert(pizza);

	        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
	        Response response = target("/Pizzas").request().post(Entity.json(pizzaCreateDto));

	        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	    }

	    @Test
	    public void testCreatePizzaWithoutName() {
	        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

	        Response response = target("/Pizzas").request().post(Entity.json(pizzaCreateDto));

	        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	    }
	    
	    @Test
	    public void testDeleteExistingPizza() {
	      Pizza pizza = new Pizza();
	      pizza.setName("Chorizo");
	      pizza.setBase("Tomate");
	      dao.insert(pizza);

	      Response response = target("/Pizzas/").path(pizza.getId().toString()).request().delete();

	      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

	      Pizza result = dao.findById(pizza.getId());
	      assertEquals(result, null);
	   }

	   @Test
	   public void testDeleteNotExistingPizza() {
	     Response response = target("/Pizzas").path(UUID.randomUUID().toString()).request().delete();
	     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	   }

	   @Test
	   public void testGetPizzaName() {
	     Pizza pizza = new Pizza();
	     pizza.setName("Chorizo");
	     pizza.setBase("Tomate");
	     dao.insert(pizza);

	     Response response = target("/Pizzas").path(pizza.getId().toString()).path("name").request().get();

	     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	     assertEquals("Chorizo", response.readEntity(String.class));
	  }

	  @Test
	  public void testGetNotExistingPizzaName() {
	    Response response = target("/Pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	  }



	
}